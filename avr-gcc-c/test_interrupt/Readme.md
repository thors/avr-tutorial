# Pin Change Interrupt

This example

* configures
    * pin0 for input
    * pin change interrupt mask for changes on pin0
    * pin 1 and pin 2 for output
    * pin 1 high per default
    * pin 2 low per default
* implements
     * an interrupt handler for pin change interrupt
         * switches on pin 2
         
The main function implements a while loop to toggle pin 1 in 250ms interval to indicate the software was successfully flashed and is "alive". When pressing T1 (the one connected to P0), the interrupt handler is triggered and toggles pin2 high, switching on LED2 on.

To switch it off again, press T2 to reset the ATTiny85.
