#include <util/delay.h>
#include <avr/interrupt.h>

/* Work in progress */

#define PIN_PCINT 0
#define LED_INTERRUPT 2
#define LED_BLINKER 1
#define PIN_CHANGE_DELAY 255
#define sbi(x,y) x |= (1 << y) // set bit
#define cbi(x,y) x &= ~(1 << y) //clear bit - using bitwise AND operator
#define tbi(x,y) x ^= _BV(y) //toggle bit - using bitwise XOR operator

//#define DEMO_MODE_SLOWDOWN
#ifdef DEMO_MODE_SLOWDOWN
uint8_t slowdown_counter;
#endif // DEMO_MODE_SLOWDOWN

#define ON_OFF_PERIODS 8
uint8_t counter;
uint8_t pinchange_delay;
uint8_t on_periods;

ISR(PCINT0_vect) {
  if (!pinchange_delay) {
	  pinchange_delay = PIN_CHANGE_DELAY;
	  if (++on_periods > ON_OFF_PERIODS) {
	    on_periods = 0;
	    counter = 1;
	  } else {
	    counter = on_periods;
	  }
    tbi(PORTB, LED_BLINKER);
  }
}

//ISR (TIMER0_COMPA_vect) {
ISR (TIM0_COMPA_vect) {
  if (pinchange_delay) {
	  pinchange_delay--;
  }
#ifdef DEMO_MODE_SLOWDOWN
  if (slowdown_counter++ > 20)  {
    slowdown_counter = 0;
#else  // DEMO_MODE_SLOWDOWN
  if (1)  {
#endif  // DEMO_MODE_SLOWDOWN
    if (--counter <= on_periods) {
      sbi(PORTB, 2);
    } else {
      cbi(PORTB, 2);
    }
    if (!counter) {
      counter = ON_OFF_PERIODS;
    }
  }
}

void init_timer() {
  // CPU freq = 8MHz = 64*125*12
  // set pre_scaler 64
  // set counter to 12
  // Timer 0 konfigurieren
  TCCR0A = (1<<WGM01); // CTC Modus
  TCCR0B = (1<<CS01) | (1<<CS00); // Prescaler 64
  OCR0A = 12; //
  // Compare Interrupt erlauben
  TIMSK |= (1<<OCIE0A);
}

int main() {
	init_timer();
	cli();
	DDRB = ((1 << LED_BLINKER) | (1 << LED_INTERRUPT)); // PORTB1 and PORTB2 as OUTPUT, others as input
	PORTB = (1 << PIN_PCINT); // Set pullup for pin 0, all outoput pins 0
	GIMSK |= (1 << PCIE);   // pin change interrupt enable
	PCMSK |= (1 << PIN_PCINT ); // pin change interrupt enabled for PB0
	sei();                  // enable interrupts
	counter = 0;
#ifdef DEMO_MODE_SLOWDOWN
	slowdown_counter = 0;
#endif // DEMO_MODE_SLOWDOWN
	on_periods = ON_OFF_PERIODS;
	while(1) //Infinite loop
	{
		_delay_ms(250);
	}

	return 0;
}
