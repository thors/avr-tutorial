; test_interrupt.s

#define SFR_OFFSET 0x20
#define PORTB 0x18
#define DDRB 0x17
#define PCMSK 0x15
#define GIMSK 0x3B
#define PCIE 5

.global main
init:
	;;; Disable interrupts
	cli
	;;; Use r16 as buffer to set intertups mask to activate pin change interrupt
	ldi r16, 0b00100000
;;; For GIMSK, as well sts as out directive should work. See Atmel ATTiny85 data sheet about I/O memory
#if 0
	sts GIMSK + SFR_OFFSET,r16
#else
	out GIMSK,r16
#endif
	;;; Use r16 as buffer to set DDRB to configure pin 1 and 2 as output, rest as input
	ldi r16, 0b00000110
	out DDRB,r16
	;;; Set pullup resistor for input pin 0
	sbi PORTB,0
	;;; Set pin change interrupt mask active for pin 0
	sbi PCMSK,0
	;;; Enable interrupts
	sei
	ret

;;; __vector_2 is the pin change interrupt
.global __vector_2
__vector_2:
	;;; Set output pin 2 high
	sbi PORTB,2
	reti

main:
	rcall init
	;;; Set output pin 2 low, 1 high
	cbi PORTB,2
	sbi PORTB,1
Start:
	sleep
	rjmp Start
