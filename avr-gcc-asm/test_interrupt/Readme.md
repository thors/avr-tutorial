# Demonstrate usage of Pin Change Interrupt

## Use pin out and pin change interrupt

This program is supposed to demonstrate how to

* configure pin 0 to trigger pin change interrupt
* configure pin 2 for output
* implement an interrupt handler for the pin change interrupt
